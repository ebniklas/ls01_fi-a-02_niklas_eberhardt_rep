import java.util.Scanner; 


public class ArrayBeispiele {
	
	public static void aufgabe1() {
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		System.out.println("Aufgabe 1:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]);
			
			if (i < zahlen.length - 1) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
	}
	
	public static void aufgabe2() {
		
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i * 2 + 1;
		}
		
		System.out.println("Aufgabe 2:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]);
			
			if (i < zahlen.length - 1) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
	}
	
	public static void aufgabe3() {
		
		Scanner tastatur = new Scanner(System.in);		
		
		int[] zahlen = new int[5];
		
		System.out.println("Aufgabe 3:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print("Bitte geben Sie eine Zahl ein: ");
			zahlen[i] = tastatur.nextInt();
		}
		
		for (int i = zahlen.length - 1; i >= 0; i--) {
			System.out.print(zahlen[i]);
			
			if (i > 0) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
	}
	
	public static void aufgabe4() {
		
		Scanner tastatur = new Scanner(System.in);		
		
		int[] zahlen = {3, 7, 12, 18, 37, 42};
		
		// a) 
		
		
		System.out.println("Aufgabe 4:");
		System.out.print("a) [");
		for (int i = 0; i < zahlen.length; i++) {
			
			System.out.print(" " + zahlen[i]);
			
			if (i == zahlen.length - 1) {
				System.out.print(" ");
			}
		}
		System.out.println("]");
		
		// b)
		
		boolean exists_12 = false;
		boolean exists_13 = false;
		
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] == 12) {
				exists_12 = true;
			}
			
			if (zahlen[i] == 13) {
				exists_13 = true;
			}
		}
		
		System.out.println("b)");
		if (exists_12) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		
		if (exists_13) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}

	public static void main(String[] args) {
		
		
		aufgabe1();
		aufgabe2();
		aufgabe3();
		aufgabe4();
	}

}