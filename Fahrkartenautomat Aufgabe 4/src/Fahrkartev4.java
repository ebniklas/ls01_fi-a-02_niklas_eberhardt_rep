import java.util.Scanner; 
	 	 
class Fahrkartev4 { 
		 
public static void main(String[] args)  { 
			 double r�ckgabebetrag; 
			 double zuZahlenderBetrag; 
			 
			 char abschluss;
			 Scanner tastatur = new Scanner(System.in);
			 

		do {
		 //double r�ckgabebetrag; 
		 //double zuZahlenderBetrag; 
		 //char abschluss;
		 zuZahlenderBetrag = fahrkartenbestellungErfassen();
		 r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		 fahrkartenAusgeben(); 
		 rueckgeldAusgeben(r�ckgabebetrag); 
		 
		 System.out.println("\nVergessen Sie nicht, den  Fahrschein\n"+ 
		 "vor Fahrtantritt entwerten zu  lassen!\n"+ 
		 "Wir w�nschen Ihnen eine gute  Fahrt."); 
		 System.out.println("Weiteres Ticket kaufen? [j/n]");
		 abschluss = tastatur.next().charAt(0);
		 } while (abschluss == 'j');
		tastatur.close();
		 }
		
public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        int option;
        double ticketpreis = 0.0;
        int anzahlDerFahrkarten;

        String[] fahrkartenBezeichnungen = {"Einzelfahrschein AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-Tageskarte ABC"};
        double[] fahrkartenPreise = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
        
        System.out.println("Option von Fahrkarten");
        
        for (int i = 0; i < fahrkartenBezeichnungen.length; i++) {
        	System.out.println(String.format("Option %d | %s | Preis: %.2f�", i + 1, fahrkartenBezeichnungen[i], fahrkartenPreise[i]));
        }
        
        System.out.println("W�hlen Sie die gew�nschte Option: ");
        option = tastatur.nextInt();
        
        ticketpreis = fahrkartenPreise[option -1];
        
        System.out.print("Anzahl der Fahrkarten: ");
        anzahlDerFahrkarten = tastatur.nextInt();
        
        
        double gesamtKosten = 0.0;
        
        if (!(anzahlDerFahrkarten >= 1 && anzahlDerFahrkarten <= 10)) {
            anzahlDerFahrkarten = 1;
            System.out.println("Anzahl der Karten ung�ltig. Fortfahren mit einer Karte");
        }
 
        gesamtKosten = (double) anzahlDerFahrkarten * ticketpreis;
        System.out.println(String.format("Gesamtpreis: %.2f�", gesamtKosten));
 
        return gesamtKosten;
    }
	  
public static double fahrkartenBezahlen(double zuZahlenderBetrag) 

	 { 
	 Scanner tastatur = new Scanner(System.in); 
	 double eingezahlterGesamtbetrag = 0.0; 
	 while(eingezahlterGesamtbetrag < zuZahlenderBetrag) 
		 { 
	 System.out.printf("%s%.2f%s\n","Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " EURO");
	  System.out.printf("%s%.2f%s%.2f%s","Eingabe (mind.  ", 0.05, " Euro, h�chstens ", 2.0, " Euro): "); 
	 double eingeworfeneM�nze = tastatur.nextDouble();
	  eingezahlterGesamtbetrag += eingeworfeneM�nze; 
	 } 
	  
	 return (eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100; 
	 } 
	
public static void fahrkartenAusgeben() 
	 { 
	 
	 System.out.println("\nFahrschein wird ausgegeben"); 
	  for (int i = 0; i < 8; i++) 
	 { 
	 System.out.print("="); 
	 try { 
	 Thread.sleep(250); 
	 } catch (InterruptedException e) { 
	 } 
	 } 
	 System.out.println("\n\n"); 
	 } 
	  
public static void rueckgeldAusgeben(double r�ckgabebetrag) 
	 { 
	 if(r�ckgabebetrag > 0.0) 
	 { 
	 System.out.printf("%s%.2f%s\n","Der R�ckgabebetrag  in H�he von ", r�ckgabebetrag / 100, " EURO"); 
	 System.out.println("wird in folgenden M�nzen  ausgezahlt:\n"); 
	  
	 String format = "%.2f%s\n"; 
	 
	 while(r�ckgabebetrag>= 200) // 2 EURO-M�nzen 
		 { 
	 System.out.printf(format, 2.0, " EURO");
	 r�ckgabebetrag -= 200; 
	 } 
	 while(r�ckgabebetrag >= 100) // 1 EURO-M�nzen 
		 { 
	 System.out.printf(format, 1.0, " EURO");
	  r�ckgabebetrag -= 100; 
	 } 
	 while(r�ckgabebetrag >= 50) // 50 CENT-M�nzen 
		 { 
	 System.out.printf(format, 0.50, " EURO");
	  r�ckgabebetrag -= 50; 
	 } 
	 while(r�ckgabebetrag >= 20) // 20 CENT-M�nzen
		 { 
	 System.out.printf(format, 0.20, " EURO");
	  r�ckgabebetrag -= 20; 
	 } 
	 while(r�ckgabebetrag >= 10) // 10 CENT-M�nzen 
		 { 
	 System.out.printf(format, 0.10, " EURO"); 
	  r�ckgabebetrag -= 10; 
	 } 
	 while(r�ckgabebetrag >= 5)// 5 CENT-M�nzen 
		 { 
	 System.out.printf(format, 0.05, " EURO"); 
	  r�ckgabebetrag -= 5; 
	 } 
	 } 
	 } 	  
	
	
}
	 