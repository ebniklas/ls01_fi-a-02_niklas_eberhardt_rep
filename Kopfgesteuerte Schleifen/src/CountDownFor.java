import java.util.Scanner;

public class CountDownFor {

	public static void main(String[] args) {
		countDownFor();
}

	private static void countDownFor() {
		Scanner myScanner = new Scanner (System.in);
		System.out.print("Geben Sie eine natürliche Zahl an: ");
		int n = myScanner.nextInt();
		
		for (int i = n ; i >0 ; i--) {
			System.out.println(i);
		}
		
	}
}